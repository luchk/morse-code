#include <iostream>
#include <string>
using namespace std;

void morse(string morseString)
{
  int size = morseString.size();
  for (size_t i = 0; i < size; i++)
  {
    // Space
    if (morseString.at(i) == 32)
    {
      cout << "       "; // English letters
    } else if (morseString.at(i) == 65 || morseString.at(i) == 97) //A
    {
      cout << "*- ";
    }else if (morseString.at(i) == 66 || morseString.at(i) == 98) //B
    {
      cout << "-*** ";
    }else if (morseString.at(i) == 67 || morseString.at(i) == 99) //C
    {
      cout << "-*-* ";
    }else if (morseString.at(i) == 68 || morseString.at(i) == 100) //D
    {
      cout << "-** ";
    }else if (morseString.at(i) == 69 || morseString.at(i) == 101) //E
    {
      cout << "* ";
    }else if (morseString.at(i) == 70 || morseString.at(i) == 102) //F
    {
      cout << "**-* ";
    }else if (morseString.at(i) == 71 || morseString.at(i) == 103) //G
    {
      cout << "--* ";
    }else if (morseString.at(i) == 72 || morseString.at(i) == 104) //H
    {
        cout << "**** ";
    }else if (morseString.at(i) == 73 || morseString.at(i) == 105) //I
    {
      cout << "** ";
    }else if (morseString.at(i) == 74 || morseString.at(i) == 106) //j
    {
      cout << "*--- ";
    }else if (morseString.at(i) == 75 || morseString.at(i) == 107) //K
    {
      cout << "-*- ";
    }else if (morseString.at(i) == 76 || morseString.at(i) == 108) //L
    {
      cout << "*-** ";
    }else if (morseString.at(i) == 77 || morseString.at(i) == 109) //M
    {
      cout << "-- ";
    }else if (morseString.at(i) == 78 || morseString.at(i) == 110) //N
    {
      cout << "-* ";
    }else if (morseString.at(i) == 79 || morseString.at(i) == 111) //O
    {
      cout << "--- ";
    }else if (morseString.at(i) == 80 || morseString.at(i) == 112) //P
    {
      cout << "*--* ";
    }else if (morseString.at(i) == 81 || morseString.at(i) == 113) //Q
    {
      cout << "--*- ";
    }else if (morseString.at(i) == 82 || morseString.at(i) == 114) //R
    {
      cout << "*-* ";
    }else if (morseString.at(i) == 83 || morseString.at(i) == 115) //S
    {
      cout << "*** ";
    }else if (morseString.at(i) == 84 || morseString.at(i) == 116) //T
    {
      cout << "- ";
    }else if (morseString.at(i) == 85 || morseString.at(i) == 117) //U
    {
      cout << "**- ";
    }else if (morseString.at(i) == 86 || morseString.at(i) == 118) //V
    {
      cout << "***- ";
    }else if (morseString.at(i) == 87 || morseString.at(i) == 119) //W
    {
      cout << "*-- ";
    }else if (morseString.at(i) == 88 || morseString.at(i) == 120) //X
    {
      cout << "-**- ";
    }else if (morseString.at(i) == 89 || morseString.at(i) == 121) //Y
    {
      cout << "-*-- ";
    }else if (morseString.at(i) == 90 || morseString.at(i) == 122) //Z
    {
      cout << "--** "; // numbers
    }else if (morseString.at(i) == 48) //0
    {
      cout << "----- ";
    }else if (morseString.at(i) == 49)//1
    {
      cout << "*---- ";
    }else if (morseString.at(i) == 50)//2
    {
      cout << "**--- ";
    }else if (morseString.at(i) == 51)//3
    {
      cout << "***-- ";
    }else if (morseString.at(i) == 52)//4
    {
      cout << "****- ";
    }else if (morseString.at(i) == 53)//5
    {
      cout << "***** ";
    }else if (morseString.at(i) == 54)//6
    {
      cout << "-**** ";
    }else if (morseString.at(i) == 45)//7
    {
      cout << "--*** ";
    }else if (morseString.at(i) == 56)//8
    {
      cout << "---** ";
    }else if (morseString.at(i) == 57)//9
    {
      cout << "----* ";
    }
  }
}


std::string str2;
int main()
{
  std::getline(std::cin, str2);
  std::cout << str2 << '\n';
  morse(str2);

  return 0;
}
