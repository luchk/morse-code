#include <iostream>
#include <string>
using namespace std;

void morse(string morseString)
{
  int size = morseString.size();
  for (size_t i = 0; i < size; i++)
  {

    switch (morseString.at(i) ) {

      case ' ':
        cout << "       ";
        break;

      case 'A':
        cout << "*- ";
        break;
      case 'a':
        cout << "*- ";
        break;

      case 'B':
        cout << "-*** ";
        break;
      case 'b':
        cout << "-*** ";
        break;

      case 'C':
        cout << "-*-* ";
        break;
      case 'c':
        cout << "-*-* ";
        break;

      case 'D':
        cout << "-** ";
        break;
      case 'd':
        cout << "-** ";
        break;

      case 'E':
        cout << "* ";
        break;
      case 'e':
        cout << "* ";
        break;

      case 'F':
        cout << "**-* ";
        break;
      case 'f':
        cout << "*- ";
        break;

      case 'G':
        cout << "--* ";
        break;
      case 'g':
        cout << "--* ";
        break;

      case 'H':
        cout << "**** ";
        break;
      case 'h':
        cout << "**** ";
        break;

      case 'I':
        cout << "** ";
        break;
      case 'i':
        cout << "** ";
        break;

      case 'J':
        cout << "*--- ";
        break;
      case 'j':
        cout << "*--- ";
        break;

      case 'K':
        cout << "-*- ";
        break;
      case 'k':
        cout << "-*- ";
        break;

      case 'L':
        cout << "*-** ";
        break;
      case 'l':
        cout << "*-** ";
        break;

      case 'M':
        cout << "-- ";
        break;
      case 'm':
        cout << "-- ";
        break;

      case 'N':
        cout << "-* ";
        break;
      case 'n':
        cout << "-* ";
        break;

      case 'O':
        cout << "--- ";
        break;
      case 'o':
        cout << "--- ";
        break;

      case 'P':
        cout << "-** ";
        break;
      case 'p':
        cout << "*--* ";
        break;

      case 'Q':
        cout << "--*- ";
        break;
      case 'q':
        cout << "--*- ";
        break;

      case 'R':
        cout << "*-* ";
        break;
      case 'r':
        cout << "*-* ";
        break;

      case 'S':
        cout << "*** ";
        break;
      case 's':
        cout << "*** ";
        break;

      case 'T':
        cout << "- ";
        break;
      case 't':
        cout << "- ";
        break;

      case 'U':
        cout << "**- ";
        break;
      case 'u':
        cout << "**- ";
        break;

      case 'V':
        cout << "***- ";
        break;
      case 'v':
        cout << "***- ";
        break;

      case 'W':
        cout << "*-- ";
        break;
      case 'w':
        cout << "*-- ";
        break;

      case 'X':
        cout << "-**- ";
        break;
      case 'x':
        cout << "-**- ";
        break;

      case 'Y':
        cout << "-*-- ";
        break;
      case 'y':
        cout << "-*-- ";
        break;

      case 'Z':
        cout << "--** ";
        break;
      case 'z':
        cout << "--** ";
        break;


      case '0':
        cout << "----- ";
        break;

      case '1':
        cout << "*---- ";
        break;

      case '2':
        cout << "**--- ";
        break;

      case '3':
        cout << "***-- ";
        break;

      case '4':
        cout << "****- ";
        break;

      case '5':
        cout << "***** ";
        break;

      case '6':
        cout << "-**** ";
        break;

      case '7':
        cout << "--*** ";
        break;

      case '8':
        cout << "---** ";
        break;

      case '9':
        cout << "----* ";
        break;
    }
  }
}


std::string str2;
int main()
{
  std::getline(std::cin, str2);
  std::cout << str2 << '\n';
  morse(str2);

  return 0;
}
