#include <iostream>
#include <string>
using namespace std;

void morse(string morseString)
{
  int size = morseString.size();
  for (size_t i = 0; i < size; i++)
  {
    // Space
    if (morseString.at(i) == 32)
    {
      cout << "       ";
    }

    // English letters
    if (morseString.at(i) == 65 || morseString.at(i) == 97) //A
    {
      cout << "*- ";
    }
    if (morseString.at(i) == 66 || morseString.at(i) == 98) //B
    {
      cout << "-*** ";
    }
    if (morseString.at(i) == 67 || morseString.at(i) == 99) //C
    {
      cout << "-*-* ";
    }
    if (morseString.at(i) == 68 || morseString.at(i) == 100) //D
    {
      cout << "-** ";
    }
    if (morseString.at(i) == 69 || morseString.at(i) == 101) //E
    {
      cout << "* ";
    }
    if (morseString.at(i) == 70 || morseString.at(i) == 102) //F
    {
      cout << "**-* ";
    }
    if (morseString.at(i) == 71 || morseString.at(i) == 103) //G
    {
      cout << "--* ";
    }
    if (morseString.at(i) == 72 || morseString.at(i) == 104) //H
    {
        cout << "**** ";
    }
    if (morseString.at(i) == 73 || morseString.at(i) == 105) //I
    {
      cout << "** ";
    }
    if (morseString.at(i) == 74 || morseString.at(i) == 106) //j
    {
      cout << "*--- ";
    }
    if (morseString.at(i) == 75 || morseString.at(i) == 107) //K
    {
      cout << "-*- ";
    }
    if (morseString.at(i) == 76 || morseString.at(i) == 108) //L
    {
      cout << "*-** ";
    }
    if (morseString.at(i) == 77 || morseString.at(i) == 109) //M
    {
      cout << "-- ";
    }
    if (morseString.at(i) == 78 || morseString.at(i) == 110) //N
    {
      cout << "-* ";
    }
    if (morseString.at(i) == 79 || morseString.at(i) == 111) //O
    {
      cout << "--- ";
    }
    if (morseString.at(i) == 80 || morseString.at(i) == 112) //P
    {
      cout << "*--* ";
    }
    if (morseString.at(i) == 81 || morseString.at(i) == 113) //Q
    {
      cout << "--*- ";
    }
    if (morseString.at(i) == 82 || morseString.at(i) == 114) //R
    {
      cout << "*-* ";
    }
    if (morseString.at(i) == 83 || morseString.at(i) == 115) //S
    {
      cout << "*** ";
    }
    if (morseString.at(i) == 84 || morseString.at(i) == 116) //T
    {
      cout << "- ";
    }
    if (morseString.at(i) == 85 || morseString.at(i) == 117) //U
    {
      cout << "**- ";
    }
    if (morseString.at(i) == 86 || morseString.at(i) == 118) //V
    {
      cout << "***- ";
    }
    if (morseString.at(i) == 87 || morseString.at(i) == 119) //W
    {
      cout << "*-- ";
    }
    if (morseString.at(i) == 88 || morseString.at(i) == 120) //X
    {
      cout << "-**- ";
    }
    if (morseString.at(i) == 89 || morseString.at(i) == 121) //Y
    {
      cout << "-*-- ";
    }
    if (morseString.at(i) == 90 || morseString.at(i) == 122) //Z
    {
      cout << "--** ";
    }

    // numbers

    if (morseString.at(i) == 48) //0
    {
      cout << "----- ";
    }
    if (morseString.at(i) == 49)//1
    {
      cout << "*---- ";
    }
    if (morseString.at(i) == 50)//2
    {
      cout << "**--- ";
    }
    if (morseString.at(i) == 51)//3
    {
      cout << "***-- ";
    }
    if (morseString.at(i) == 52)//4
    {
      cout << "****- ";
    }
    if (morseString.at(i) == 53)//5
    {
      cout << "***** ";
    }
    if (morseString.at(i) == 54)//6
    {
      cout << "-**** ";
    }
    if (morseString.at(i) == 45)//7
    {
      cout << "--*** ";
    }
    if (morseString.at(i) == 56)//8
    {
      cout << "---** ";
    }
    if (morseString.at(i) == 57)//9
    {
      cout << "----* ";
    }

  }
}


std::string str2;
int main()
{
  std::getline(std::cin, str2);
  std::cout << str2 << '\n';
  morse(str2);

  return 0;
}
